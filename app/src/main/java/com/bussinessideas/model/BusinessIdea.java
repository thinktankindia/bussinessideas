
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BusinessIdea implements Parcelable {

    @SerializedName("idea")
    @Expose
    private List<Idea> idea = new ArrayList<>();
    @SerializedName("contact_us")
    @Expose
    private ContactUs contactUs;

    /**
     * @return The idea
     */
    public List<Idea> getIdea() {
        return idea;
    }

    /**
     * @param idea The idea
     */
    public void setIdea(List<Idea> idea) {
        this.idea = idea;
    }

    /**
     * @return The contactUs
     */
    public ContactUs getContactUs() {
        return contactUs;
    }

    /**
     * @param contactUs The contact_us
     */
    public void setContactUs(ContactUs contactUs) {
        this.contactUs = contactUs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.idea);
        dest.writeParcelable(this.contactUs, flags);
    }

    public BusinessIdea() {
    }

    protected BusinessIdea(Parcel in) {
        this.idea = new ArrayList<>();
        in.readList(this.idea, Idea.class.getClassLoader());
        this.contactUs = in.readParcelable(ContactUs.class.getClassLoader());
    }

    public static final Parcelable.Creator<BusinessIdea> CREATOR = new Parcelable.Creator<BusinessIdea>() {
        @Override
        public BusinessIdea createFromParcel(Parcel source) {
            return new BusinessIdea(source);
        }

        @Override
        public BusinessIdea[] newArray(int size) {
            return new BusinessIdea[size];
        }
    };
}
