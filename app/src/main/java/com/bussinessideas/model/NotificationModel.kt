package com.bussinessideas.model

data class NotificationModel(var content: String,
                             var title: String,
                             var imageUrl: String,
                             var gameUrl: String)