
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactUs implements Parcelable {

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("whatsapp_num")
    @Expose
    private String whatsappNum;
    @SerializedName("fb_link")
    @Expose
    private String fbLink;

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The whatsappNum
     */
    public String getWhatsappNum() {
        return whatsappNum;
    }

    /**
     * @param whatsappNum The whatsapp_num
     */
    public void setWhatsappNum(String whatsappNum) {
        this.whatsappNum = whatsappNum;
    }

    /**
     * @return The fbLink
     */
    public String getFbLink() {
        return fbLink;
    }

    /**
     * @param fbLink The fb_link
     */
    public void setFbLink(String fbLink) {
        this.fbLink = fbLink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.whatsappNum);
        dest.writeString(this.fbLink);
    }

    public ContactUs() {
    }

    protected ContactUs(Parcel in) {
        this.phone = in.readString();
        this.email = in.readString();
        this.whatsappNum = in.readString();
        this.fbLink = in.readString();
    }

    public static final Parcelable.Creator<ContactUs> CREATOR = new Parcelable.Creator<ContactUs>() {
        @Override
        public ContactUs createFromParcel(Parcel source) {
            return new ContactUs(source);
        }

        @Override
        public ContactUs[] newArray(int size) {
            return new ContactUs[size];
        }
    };
}
