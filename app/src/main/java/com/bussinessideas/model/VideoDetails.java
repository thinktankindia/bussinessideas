package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoDetails implements Parcelable {

    String VideoName;
    String VideoDesc;
    String URL;
    String VideoId;

    public String getVideoName() {
        return VideoName;
    }

    public void setVideoName(String videoName) {
        VideoName = videoName;
    }

    public String getVideoDesc() {
        return VideoDesc;
    }

    public void setVideoDesc(String videoDesc) {
        VideoDesc = videoDesc;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getVideoId() {
        return VideoId;
    }

    public void setVideoId(String videoId) {
        VideoId = videoId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.VideoName);
        dest.writeString(this.VideoDesc);
        dest.writeString(this.URL);
        dest.writeString(this.VideoId);
    }

    public VideoDetails() {
    }

    protected VideoDetails(Parcel in) {
        this.VideoName = in.readString();
        this.VideoDesc = in.readString();
        this.URL = in.readString();
        this.VideoId = in.readString();
    }

    public static final Parcelable.Creator<VideoDetails> CREATOR = new Parcelable.Creator<VideoDetails>() {
        @Override
        public VideoDetails createFromParcel(Parcel source) {
            return new VideoDetails(source);
        }

        @Override
        public VideoDetails[] newArray(int size) {
            return new VideoDetails[size];
        }
    };
}