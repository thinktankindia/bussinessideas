
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Idea implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_long")
    @Expose
    private String titleLong;
    @SerializedName("idea_desc")
    @Expose
    private String ideaDesc;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("tags")
    @Expose
    private List<String> tags = new ArrayList<String>();
    @SerializedName("how_to_start")
    @Expose
    private String howToStart;
    @SerializedName("investment")
    @Expose
    private Investment investment;
    @SerializedName("expected_income")
    @Expose
    private ExpectedIncome expectedIncome;
    @SerializedName("favorable_location")
    @Expose
    private String favorableLocation;
    @SerializedName("government_aid")
    @Expose
    private GovernmentAid governmentAid;
    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;
    @SerializedName("extras")
    @Expose
    private List<String> extras = new ArrayList<String>();

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The titleLong
     */
    public String getTitleLong() {
        return titleLong;
    }

    /**
     * @param titleLong The title_long
     */
    public void setTitleLong(String titleLong) {
        this.titleLong = titleLong;
    }

    /**
     * @return The ideaDesc
     */
    public String getIdeaDesc() {
        return ideaDesc;
    }

    /**
     * @param ideaDesc The idea_desc
     */
    public void setIdeaDesc(String ideaDesc) {
        this.ideaDesc = ideaDesc;
    }

    /**
     * @return The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @param summary The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * @return The tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * @param tags The tags
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return The howToStart
     */
    public String getHowToStart() {
        return howToStart;
    }

    /**
     * @param howToStart The how_to_start
     */
    public void setHowToStart(String howToStart) {
        this.howToStart = howToStart;
    }

    /**
     * @return The investment
     */
    public Investment getInvestment() {
        return investment;
    }

    /**
     * @param investment The investment
     */
    public void setInvestment(Investment investment) {
        this.investment = investment;
    }

    /**
     * @return The expectedIncome
     */
    public ExpectedIncome getExpectedIncome() {
        return expectedIncome;
    }

    /**
     * @param expectedIncome The expected_income
     */
    public void setExpectedIncome(ExpectedIncome expectedIncome) {
        this.expectedIncome = expectedIncome;
    }

    /**
     * @return The favorableLocation
     */
    public String getFavorableLocation() {
        return favorableLocation;
    }

    /**
     * @param favorableLocation The favorable_location
     */
    public void setFavorableLocation(String favorableLocation) {
        this.favorableLocation = favorableLocation;
    }

    /**
     * @return The governmentAid
     */
    public GovernmentAid getGovernmentAid() {
        return governmentAid;
    }

    /**
     * @param governmentAid The government_aid
     */
    public void setGovernmentAid(GovernmentAid governmentAid) {
        this.governmentAid = governmentAid;
    }

    /**
     * @return The extras
     */
    public List<String> getExtras() {
        return extras;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     * @param extras The extras
     */
    public void setExtras(List<String> extras) {
        this.extras = extras;
    }

    public Idea() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeString(this.titleLong);
        dest.writeString(this.ideaDesc);
        dest.writeString(this.summary);
        dest.writeStringList(this.tags);
        dest.writeString(this.howToStart);
        dest.writeParcelable(this.investment, flags);
        dest.writeParcelable(this.expectedIncome, flags);
        dest.writeString(this.favorableLocation);
        dest.writeParcelable(this.governmentAid, flags);
        dest.writeString(this.imgUrl);
        dest.writeStringList(this.extras);
    }

    protected Idea(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.titleLong = in.readString();
        this.ideaDesc = in.readString();
        this.summary = in.readString();
        this.tags = in.createStringArrayList();
        this.howToStart = in.readString();
        this.investment = in.readParcelable(Investment.class.getClassLoader());
        this.expectedIncome = in.readParcelable(ExpectedIncome.class.getClassLoader());
        this.favorableLocation = in.readString();
        this.governmentAid = in.readParcelable(GovernmentAid.class.getClassLoader());
        this.imgUrl = in.readString();
        this.extras = in.createStringArrayList();
    }

    public static final Creator<Idea> CREATOR = new Creator<Idea>() {
        @Override
        public Idea createFromParcel(Parcel source) {
            return new Idea(source);
        }

        @Override
        public Idea[] newArray(int size) {
            return new Idea[size];
        }
    };
}
