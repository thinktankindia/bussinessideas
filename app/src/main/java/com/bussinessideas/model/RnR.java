
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RnR implements Parcelable {

    @SerializedName("book_name")
    @Expose
    private String bookName;
    @SerializedName("chapter")
    @Expose
    private List<Chapter> chapter = null;

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public List<Chapter> getChapter() {
        return chapter;
    }

    public void setChapter(List<Chapter> chapter) {
        this.chapter = chapter;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bookName);
        dest.writeTypedList(this.chapter);
    }

    public RnR() {
    }

    protected RnR(Parcel in) {
        this.bookName = in.readString();
        this.chapter = in.createTypedArrayList(Chapter.CREATOR);
    }

    public static final Parcelable.Creator<RnR> CREATOR = new Parcelable.Creator<RnR>() {
        @Override
        public RnR createFromParcel(Parcel source) {
            return new RnR(source);
        }

        @Override
        public RnR[] newArray(int size) {
            return new RnR[size];
        }
    };
}
