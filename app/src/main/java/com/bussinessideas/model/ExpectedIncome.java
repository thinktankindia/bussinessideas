
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExpectedIncome implements Parcelable {

    @SerializedName("time_required_to_setup")
    @Expose
    private String timeRequiredToSetup;
    @SerializedName("expected_monthly_income")
    @Expose
    private String expectedMonthlyIncome;

    /**
     * @return The timeRequiredToSetup
     */
    public String getTimeRequiredToSetup() {
        return timeRequiredToSetup;
    }

    /**
     * @param timeRequiredToSetup The time_required_to_setup
     */
    public void setTimeRequiredToSetup(String timeRequiredToSetup) {
        this.timeRequiredToSetup = timeRequiredToSetup;
    }

    /**
     * @return The expectedMonthlyIncome
     */
    public String getExpectedMonthlyIncome() {
        return expectedMonthlyIncome;
    }

    /**
     * @param expectedMonthlyIncome The expected_monthly_income
     */
    public void setExpectedMonthlyIncome(String expectedMonthlyIncome) {
        this.expectedMonthlyIncome = expectedMonthlyIncome;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.timeRequiredToSetup);
        dest.writeString(this.expectedMonthlyIncome);
    }

    public ExpectedIncome() {
    }

    protected ExpectedIncome(Parcel in) {
        this.timeRequiredToSetup = in.readString();
        this.expectedMonthlyIncome = in.readString();
    }

    public static final Parcelable.Creator<ExpectedIncome> CREATOR = new Parcelable.Creator<ExpectedIncome>() {
        @Override
        public ExpectedIncome createFromParcel(Parcel source) {
            return new ExpectedIncome(source);
        }

        @Override
        public ExpectedIncome[] newArray(int size) {
            return new ExpectedIncome[size];
        }
    };
}
