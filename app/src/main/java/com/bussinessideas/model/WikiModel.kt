package com.bussinessideas.model

object WikiModel {
    data class Result(val query: Query)
    data class Query(val searchinfo: SearchInfo, val search: List<Search>)
    data class SearchInfo(val totalhits: Int)
    data class Search(
            val ns: Int,
            val title: String,
            val pageid: Int,
            val size: Int,
            val wordcount: Int,
            val snippet: String,
            val timestamp: String
    )
}