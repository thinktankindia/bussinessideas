package com.bussinessideas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

object BiModel {

    @Parcelize
    data class Result (
            @SerializedName("idea") val idea : List<Idea>,
            @SerializedName("contact_us") val contact_us : Contact_us
    ): Parcelable

    @Parcelize
    data class Investment (

            @SerializedName("time") val time : String,
            @SerializedName("money") val money : String,
            @SerializedName("manpower") val manpower : String,
            @SerializedName("space") val space : String,
            @SerializedName("can_be_done_from_home") val can_be_done_from_home : Boolean
    ): Parcelable

    @Parcelize
    data class Idea (

            @SerializedName("id") val id : Int,
            @SerializedName("imgUrl") val imgUrl : String,
            @SerializedName("title") val title : String,
            @SerializedName("title_long") val title_long : String,
            @SerializedName("idea_desc") val idea_desc : String,
            @SerializedName("summary") val summary : String,
            @SerializedName("tags") val tags : List<String>,
            @SerializedName("how_to_start") val how_to_start : String,
            @SerializedName("investment") val investment : Investment,
            @SerializedName("expected_income") val expected_income : Expected_income,
            @SerializedName("favorable_location") val favorable_location : String,
            @SerializedName("government_aid") val government_aid : Government_aid,
            @SerializedName("contact_us") val contact_us : Contact_us,
            @SerializedName("extras") val extras : List<String>
    ): Parcelable

    @Parcelize
    data class Government_aid (

            @SerializedName("is_provided") val is_provided : Boolean,
            @SerializedName("detail") val detail : String,
            @SerializedName("contact") val contact : String
    ): Parcelable

    @Parcelize
    data class Expected_income (

            @SerializedName("time_required_to_setup") val time_required_to_setup : String,
            @SerializedName("expected_monthly_income") val expected_monthly_income : String
    ) : Parcelable

    @Parcelize
    data class Contact_us (

            @SerializedName("phone") val phone : String,
            @SerializedName("email") val email : String,
            @SerializedName("whatsapp_num") val whatsapp_num : String,
            @SerializedName("fb_link") val fb_link : String
    ): Parcelable
}