
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GovernmentAid implements Parcelable {

    @SerializedName("is_provided")
    @Expose
    private Boolean isProvided;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("contact")
    @Expose
    private String contact;

    /**
     *
     * @return
     *     The isProvided
     */
    public Boolean isProvided() {
        return isProvided;
    }

    /**
     *
     * @param isProvided
     *     The is_provided
     */
    public void setIsProvided(Boolean isProvided) {
        this.isProvided = isProvided;
    }

    /**
     *
     * @return
     *     The detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     *
     * @param detail
     *     The detail
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     *
     * @return
     *     The contact
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     *     The contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.isProvided);
        dest.writeString(this.detail);
        dest.writeString(this.contact);
    }

    public GovernmentAid() {
    }

    protected GovernmentAid(Parcel in) {
        this.isProvided = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.detail = in.readString();
        this.contact = in.readString();
    }

    public static final Parcelable.Creator<GovernmentAid> CREATOR = new Parcelable.Creator<GovernmentAid>() {
        @Override
        public GovernmentAid createFromParcel(Parcel source) {
            return new GovernmentAid(source);
        }

        @Override
        public GovernmentAid[] newArray(int size) {
            return new GovernmentAid[size];
        }
    };
}
