
package com.bussinessideas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Investment implements Parcelable {

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("money")
    @Expose
    private String money;
    @SerializedName("manpower")
    @Expose
    private String manpower;
    @SerializedName("space")
    @Expose
    private String space;
    @SerializedName("can_be_done_from_home")
    @Expose
    private Boolean canBeDoneFromHome;

    /**
     * @return The time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return The money
     */
    public String getMoney() {
        return money;
    }

    /**
     * @param money The money
     */
    public void setMoney(String money) {
        this.money = money;
    }

    /**
     * @return The manpower
     */
    public String getManpower() {
        return manpower;
    }

    /**
     * @param manpower The manpower
     */
    public void setManpower(String manpower) {
        this.manpower = manpower;
    }

    /**
     * @return The space
     */
    public String getSpace() {
        return space;
    }

    /**
     * @param space The space
     */
    public void setSpace(String space) {
        this.space = space;
    }

    /**
     * @return The canBeDoneFromHome
     */
    public Boolean getCanBeDoneFromHome() {
        return canBeDoneFromHome;
    }

    /**
     * @param canBeDoneFromHome The can_be_done_from_home
     */
    public void setCanBeDoneFromHome(Boolean canBeDoneFromHome) {
        this.canBeDoneFromHome = canBeDoneFromHome;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.time);
        dest.writeString(this.money);
        dest.writeString(this.manpower);
        dest.writeString(this.space);
        dest.writeValue(this.canBeDoneFromHome);
    }

    public Investment() {
    }

    protected Investment(Parcel in) {
        this.time = in.readString();
        this.money = in.readString();
        this.manpower = in.readString();
        this.space = in.readString();
        this.canBeDoneFromHome = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Investment> CREATOR = new Parcelable.Creator<Investment>() {
        @Override
        public Investment createFromParcel(Parcel source) {
            return new Investment(source);
        }

        @Override
        public Investment[] newArray(int size) {
            return new Investment[size];
        }
    };
}
