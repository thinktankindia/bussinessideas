package com.bussinessideas.activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bussinessideas.R;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.LogUtils;
import com.bussinessideas.util.SharedPrefUtils;

public class SplashScreenActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private ImageView image;
    private TextView tv_business_idea;
    private RelativeLayout rlSplashScreen;
    private static final String TAG = LogUtils.makeLogTag(SplashScreenActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setBackgroundDrawable(getApplicationContext().getResources().getDrawable(R.drawable.splash_screen));
        setContentView(R.layout.view_splash);

        rlSplashScreen = findViewById(R.id.rl_splash_screen);
        image = findViewById(R.id.imageView);
        tv_business_idea = findViewById(R.id.tv_business_idea);

        //Set background of the view. This has not been set in the xml to avoid crash.
        //setHomeBackground(rlSplashScreen);

        //YoYo.with(Techniques.BounceInUp).duration(3000).playOn(image);

        ObjectAnimator.ofFloat(image, "translationY", image.getMeasuredHeight(), -30, 20, 0).setDuration(3000).start();
        ObjectAnimator.ofFloat(tv_business_idea, "alpha", 1, 0.5f, 1).setDuration(3000).start();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i;
                if (SharedPrefUtils.getInstance().getInt(SharedPrefUtils.Key.SELECTED_LANGUAGE) == -1) {
                    i = new Intent(SplashScreenActivity.this, BusinessIdeaLandingActivity.class);
                } else {
                    i = new Intent(SplashScreenActivity.this, BusinessIdeaLandingActivity.class);
                }
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void setHomeBackground(RelativeLayout rlSplashScreen) {
        try {
            DisplayMetrics mDisplayMetrics = new DisplayMetrics();
            Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            display.getMetrics(mDisplayMetrics);

            AppController.getInstance();
            BitmapDrawable backgroundDrawable = new BitmapDrawable(getResources(), AppController.getCompressedBackground(R.drawable.splash_gradient
                    , mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, Bitmap.Config.RGB_565));

            rlSplashScreen.setBackground(backgroundDrawable);

        } catch (Throwable exec) {//catching throwable to avoid OutOfMemory error Don't Do Anything inside this block
            LogUtils.error(TAG, exec);
        }
    }

}