package com.bussinessideas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bussinessideas.R;
import com.bussinessideas.fragment.ContactUsFragment;
import com.bussinessideas.tracker.TrackingHelper;
import com.bussinessideas.util.AppConstants;
import com.bussinessideas.util.AppController;

import static com.bussinessideas.util.AppConstants.DETAIL_TYPE;
import static com.bussinessideas.util.AppConstants.RNR;
import static com.bussinessideas.util.AppConstants.TIPS;

public class BusinessIdeaLandingActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout rlBusinessIdea;
    private RelativeLayout rlTopIdeas;
    private RelativeLayout rlOtherApps;
    private RelativeLayout rlContactUs;
    private RelativeLayout rlBlock3;
    private RelativeLayout rlBlock4;
    private RelativeLayout rlVideos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.setLocale();
        setContentView(R.layout.activity_business_idea_landing);
        initViews();
    }

    private void initViews() {
        rlBusinessIdea = findViewById(R.id.rl_business_idea);
        rlTopIdeas = findViewById(R.id.rl_top_ideas);
        rlOtherApps = findViewById(R.id.rl_videos);
        rlContactUs = findViewById(R.id.rl_contact_us);
        rlBlock3 = findViewById(R.id.rl_block3);
        rlBlock4 = findViewById(R.id.rl_block4);
        rlVideos = findViewById(R.id.rl_videos);
        ImageView iv_share = (ImageView) findViewById(R.id.iv_share);

        ImageView ivSelectLanguage = (ImageView) findViewById(R.id.iv_select_language);

        //Set on click listener
        rlBusinessIdea.setOnClickListener(this);
        rlTopIdeas.setOnClickListener(this);
        rlOtherApps.setOnClickListener(this);
        rlContactUs.setOnClickListener(this);
        rlBlock3.setOnClickListener(this);
        rlBlock4.setOnClickListener(this);
        rlVideos.setOnClickListener(this);

        ivSelectLanguage.setOnClickListener(this);
        iv_share.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_business_idea:
                showBusinessIdeas();
                return;
            case R.id.rl_top_ideas:
                showTopIdeas();
                return;
            case R.id.rl_contact_us:
                openContactUsFragment();
                return;
            case R.id.rl_block3:
                openRagsAndRiches(RNR);
                return;
            case R.id.rl_block4:
                openRagsAndRiches(TIPS);
                return;
            case R.id.iv_select_language:
                changeLanguage();
                return;
            case R.id.iv_share:
                shareIdea();
                return;
            case R.id.rl_videos:
                showVideo();
            default:
        }
    }

    private void showVideo() {
        Intent i = new Intent(BusinessIdeaLandingActivity.this, ChannelActivity.class);
        startActivity(i);
    }

    private void openRagsAndRiches(int type) {
        Intent i = new Intent(BusinessIdeaLandingActivity.this, BusinessIdeaRnRActivity.class);
        i.putExtra(DETAIL_TYPE, type);
        startActivity(i);
    }

    private void openContactUsFragment() {
        ContactUsFragment frag = new ContactUsFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fl_landing, frag, AppConstants.CONTACT_US_FRAG)
                .commitAllowingStateLoss();
    }

    private void changeLanguage() {
        if (AppController.getInstance().isNetworkAvailable()) {
            //Intent i = new Intent(BusinessIdeaLandingActivity.this, SelectLanguageActivity.class);
            //startActivity(i);
            //tracking
            TrackingHelper.changeLanguageIconClicked();
            finish();
        } else {
            Toast.makeText(this, getResources().getString(R.string.STR_NEED_INTERNET_TO_CHANGE_LANGUAGE), Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Opens Listing Activity with Top Ideas.
     */
    private void showTopIdeas() {
        int[] top_ideas = getResources().getIntArray(R.array.top_ideas);
        Intent i = new Intent(BusinessIdeaLandingActivity.this, BusinessIdeasListingActivity.class);
        i.putExtra(AppConstants.TOP_IDEAS_IDS, top_ideas);
        startActivity(i);
    }

    private void showBusinessIdeas() {
        Intent i = new Intent(BusinessIdeaLandingActivity.this, BusinessIdeasListingActivity.class);
        startActivity(i);
    }

    private void shareIdea() {
        AppController.getInstance().shareApp(this);
    }

    @Override
    public void onBackPressed() {
        if (AppController.isFragmentUIActive(getSupportFragmentManager().findFragmentByTag(AppConstants.CONTACT_US_FRAG))) {
            getSupportFragmentManager().popBackStack();
        } else {
            this.finish();
        }
    }
}
