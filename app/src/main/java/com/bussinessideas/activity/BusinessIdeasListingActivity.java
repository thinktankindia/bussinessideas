package com.bussinessideas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bussinessideas.R;
import com.bussinessideas.adapter.BusinessIdeasListingAdapter;
import com.bussinessideas.ads.AdBuilder;
import com.bussinessideas.model.BusinessIdea;
import com.bussinessideas.model.Idea;
import com.bussinessideas.tracker.TrackingHelper;
import com.bussinessideas.util.AppConstants;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.ArrayUtils;
import com.bussinessideas.util.GsonUtils;
import com.google.android.gms.ads.AdView;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import io.paperdb.Paper;

import static com.bussinessideas.util.AppConstants.IDEA_LISTING_DATA;

public class BusinessIdeasListingActivity extends BaseActivity implements AdBuilder.AdBuilderListener, View.OnClickListener {
    public static final String IDEA_DATA = "idea_data";
    public static final int ITEMS_PER_AD = 5;
    private AdView mAdView;
    private AdBuilder adBuilder;
    private boolean backPressed = false;
    private boolean backClickedFromInterstetialAd = false;
    private List<Idea> ideas;
    private int[] topIdeas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.setLocale();
        topIdeas = getIntent().getIntArrayExtra(AppConstants.TOP_IDEAS_IDS);
        setContentView(R.layout.activity_business_ideas_listing);
        initIdeasList();
        initTopIdeas();
        initViews();

        //Show ads.
        adBuilder = new AdBuilder(mAdView, this);
        adBuilder.showAds();

        //Load Interstetial Ads
        adBuilder.loadInterstitial(getString(R.string.interstitial_ad_unit_listing));
        TrackingHelper.listingPageLoaded();
    }


    private void initViews() {
        RecyclerView rvIdea = (RecyclerView) findViewById(R.id.rv_idea);
        mAdView = findViewById(R.id.adView);
        ImageView ivBack = findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        TextView tvListingHeaderText = findViewById(R.id.tv_listing_header_text);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        Collections.shuffle(ideas);
        BusinessIdeasListingAdapter adapter = new BusinessIdeasListingAdapter(this, ideas);
        rvIdea.setLayoutManager(mLayoutManager);
        rvIdea.setItemAnimator(new DefaultItemAnimator());
        rvIdea.setAdapter(adapter);
        tvListingHeaderText.setText(getResources().getString(R.string.business_idea));

    }


    public void handleIdeaClicked(Object tag) {
        int ideaPosition = (Integer) tag;
        Idea idea = ideas.get(ideaPosition);

        //tracking
        TrackingHelper.onListingPageItemClicked(idea.getTitle(), ideaPosition);

        //Start Detail Activity Intent.
        Intent intent = new Intent(this, BusinessIdeaDetailActivity.class);
        intent.putExtra(IDEA_DATA, idea);
        startActivity(intent);
    }

    private void initIdeasList() {
        String data = Paper.book().read(IDEA_LISTING_DATA);
        if (data == null || data.isEmpty()) {
            data = AppController.getInstance().getIdeasDataJSONFromAsset();
        }
        BusinessIdea businessIdea = GsonUtils.getInstance().deserializeJSON(data, BusinessIdea.class);
        ideas = businessIdea.getIdea();
    }

    /**
     * Below method is remove all the ideas from the list Except Top Ideas.
     */
    private void initTopIdeas() {
        if (ideas != null && !ideas.isEmpty() && ArrayUtils.isNotEmpty(topIdeas)) {
            for (Iterator<Idea> iterator = ideas.iterator(); iterator.hasNext(); ) {
                int id = iterator.next().getId();
                if (!ArrayUtils.contains(topIdeas, id)) {
                    iterator.remove();
                }
            }
        }
    }


    @Override
    public void onPause() {
        adBuilder.pauseAd();
        super.onPause();
    }

    @Override
    public void onResume() {
        adBuilder.resumeAd();
        super.onResume();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (backClickedFromInterstetialAd) {
            onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        adBuilder.destroyAd();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (backPressed || !adBuilder.isInterstitialAdLoaded()) {
            super.onBackPressed();
        } else {
            backPressed = true;
            adBuilder.showInterstitial();
        }
    }

    @Override
    public void onAdClosed() {
        backClickedFromInterstetialAd = true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back) {
            onBackPressed();
        }
    }
}