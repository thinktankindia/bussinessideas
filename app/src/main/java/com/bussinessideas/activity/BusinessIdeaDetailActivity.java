package com.bussinessideas.activity;


import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bussinessideas.R;
import com.bussinessideas.adapter.DetailPagerAdapter;
import com.bussinessideas.ads.AdBuilder;
import com.bussinessideas.fragment.DetailDescriptionFragment;
import com.bussinessideas.fragment.DetailEarningsFragment;
import com.bussinessideas.fragment.DetailSummaryFragment;
import com.bussinessideas.model.Idea;
import com.bussinessideas.tracker.TrackingHelper;
import com.bussinessideas.util.AppController;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;

import java.util.Date;
import java.util.Map;

import static com.bussinessideas.activity.BusinessIdeasListingActivity.IDEA_DATA;
import static com.bussinessideas.tracker.TrackingHelper.getDefaultParamsMap;
import static com.bussinessideas.util.AppConstants.MIN_TIME_TO_DISPLAY_INTERSTETIAL_AD;

public class BusinessIdeaDetailActivity extends AppCompatActivity implements View.OnClickListener, AdBuilder.AdBuilderListener, DetailSummaryFragment.OnDetailSummaryFragmentInteraction, DetailEarningsFragment.OnDetailEarningsFragmentInteraction, DetailDescriptionFragment.OnDetailDescriptionFragmentInteraction {

    private static final String EVENT_DETAIL_PAGE = "DETAIL_PAGE_LOADED";


    private AdView mAdView;
    public NativeExpressAdView nativeExpressAdView;
    private Idea idea;
    private AdBuilder adBuilder;
    private long pageInitTime = 0;
    private TextView tvIdeaHeader;
    private boolean interstetialShowed = false;
    Map<String, String> params = getDefaultParamsMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageInitTime = new Date().getTime();
        idea = getIntent().getParcelableExtra(IDEA_DATA);
        setContentView(R.layout.activity_business_ideas_detail);
        initViews();

        //Tracking
        FlurryAgent.logEvent(EVENT_DETAIL_PAGE, params, true);

        //Show ads.
        adBuilder = new AdBuilder(mAdView, this);
        adBuilder.showAds();

        //Load Interstetial Ad
        adBuilder.loadInterstitial(getString(R.string.interstitial_ad_unit_detail));

        ViewPager vpPager = findViewById(R.id.vpPager);
        DetailPagerAdapter adapter = new DetailPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapter);

    }


    private void initViews() {
        ImageView ivBack = findViewById(R.id.iv_back);
        ImageView ivShare = findViewById(R.id.iv_share);
        mAdView = findViewById(R.id.adView);
        nativeExpressAdView = findViewById(R.id.adView_native_detail);
        tvIdeaHeader = findViewById(R.id.tv_idea_header);

        //Set on click listener
        ivBack.setOnClickListener(this);
        ivShare.setOnClickListener(this);

        //setTitle
        tvIdeaHeader.setText(idea.getTitle());
    }

    @Override
    public void onPause() {
        super.onPause();
        adBuilder.pauseAd();
    }

    @Override
    public void onResume() {
        super.onResume();
        adBuilder.resumeAd();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adBuilder.destroyAd();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back) {
            super.onBackPressed();
        } else if (v.getId() == R.id.iv_share) {
            shareIdea();
        }
    }

    private void shareIdea() {
        AppController.getInstance().shareApp(this);
        TrackingHelper.shareIconClicked();
    }

    @Override
    public void onBackPressed() {
        long clickTime = new Date().getTime();
        if (interstetialShowed || clickTime - pageInitTime < MIN_TIME_TO_DISPLAY_INTERSTETIAL_AD) {
            super.onBackPressed();
        } else {
            adBuilder.showInterstitial();
            interstetialShowed = true;
            FlurryAgent.endTimedEvent(EVENT_DETAIL_PAGE);
            finish();
        }
    }


    @Override
    public void onAdClosed() {
        finish();
    }


    @Override
    public Idea getIdea() {
        return idea;
    }
}
