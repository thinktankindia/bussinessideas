package com.bussinessideas.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bussinessideas.R
import com.bussinessideas.adapter.VideoAdapter
import com.bussinessideas.model.VideoDetails
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

open class VideoActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {
    private lateinit var youTubePlayerView: YouTubePlayerView
    private lateinit var rvVideo: RecyclerView
    private lateinit var videoList: List<VideoDetails>
    private var videoId: String? = null
    private lateinit var youTubePlayer: YouTubePlayer
    private lateinit var adapter: VideoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        youTubePlayerView = findViewById(R.id.you_tube_view)
        rvVideo = findViewById(R.id.rv_video)
        val backButton: ImageView = findViewById(R.id.iv_back)

        youTubePlayerView.initialize(getString(R.string.GOOGLE_API_KEY), this)
        backButton.setOnClickListener { super.onBackPressed() }

        videoList = intent.getParcelableArrayListExtra("video_list")!!
        videoId = intent.getStringExtra("videoId")

        rvVideo.layoutManager = LinearLayoutManager(this)
        adapter = VideoAdapter(getFormattedList(videoId), this)
        rvVideo.adapter = this.adapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RECOVERY_REQUEST) {
            youTubePlayerProvider!!.initialize(getString(R.string.GOOGLE_API_KEY), this)
        }
    }

    override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer, b: Boolean) {
        val bundle = intent.extras
        val videoId = bundle!!.getString("videoId")
        this.youTubePlayer = youTubePlayer
        showVideo(videoId)
    }

    fun showVideo(videoId: String?) {
        youTubePlayer.cueVideo(videoId)
        youTubePlayer.play()
        adapter.updateList(getFormattedList(videoId).shuffled())
        rvVideo.smoothScrollToPosition(0)
    }


    override fun onInitializationFailure(provider: YouTubePlayer.Provider, youTubeInitializationResult: YouTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show()
        } else {
            Toast.makeText(this, "Error Initializing Youtube Player", Toast.LENGTH_LONG).show()
        }
    }

    private fun getFormattedList(videoId: String?): List<VideoDetails> {
        return videoList.filter { v -> v.videoId != videoId }
    }

    private val youTubePlayerProvider: YouTubePlayer.Provider?
        get() = youTubePlayerView

    companion object {
        private const val RECOVERY_REQUEST = 1
    }
}