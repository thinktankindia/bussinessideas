package com.bussinessideas.activity

import android.content.Context
import android.content.Intent
import android.content.res.TypedArray
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bussinessideas.R
import com.bussinessideas.fragment.NetworkDialog
import com.bussinessideas.model.BiModel
import com.bussinessideas.model.BusinessIdea
import com.bussinessideas.network.BiService.Companion.create
import com.bussinessideas.tracker.TrackingHelper
import com.bussinessideas.util.*
import io.paperdb.Paper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class SelectLanguageActivity : BaseActivity(), SmoothCheckBox.OnCheckedChangeListener, View.OnClickListener {
    private lateinit var languages: Array<String>
    private var img: TypedArray? = null
    private lateinit var llLanguageList: LinearLayout
    private lateinit var confirmButton: TextView
    private var dialogFragment: NetworkDialog? = null
    private var disposable: Disposable? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_language)
        initViews()
    }

    private fun initViews() {
        languages = resources.getStringArray(R.array.supported_languages)
        img = resources.obtainTypedArray(R.array.flags)
        llLanguageList = findViewById(R.id.ll_language_list)
        confirmButton = findViewById(R.id.tv_confirm_language)
        confirmButton.setOnClickListener(this)
        val inflater = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        for (i in languages.indices) { //inflate view.
            val view = inflater.inflate(R.layout.view_language, null)
            //Init views
            val lang = view.findViewById<TextView>(R.id.tv_language_name)
            val flag = view.findViewById<ImageView>(R.id.iv_flag)
            val checkBox: SmoothCheckBox = view.findViewById(R.id.scb_checkBox)
            checkBox.setOnCheckedChangeListener(this)
            checkBox.tag = i
            //Populate data
            lang.text = languages[i]
            flag.setImageResource(img!!.getResourceId(i, 0))
            llLanguageList.addView(view)
        }
    }

    override fun onCheckedChanged(checkBox: SmoothCheckBox, isChecked: Boolean) {
        AppController.setLocale()
    }

    override fun onClick(v: View) {
        changeLanguage()
    }

    private fun changeLanguage() {
        if (SharedPrefUtils.getInstance().getInt(SharedPrefUtils.Key.SELECTED_LANGUAGE) == -1) {
            Toast.makeText(this, "Please select a language", Toast.LENGTH_SHORT).show()
        } else {
            SharedPrefUtils.getInstance().put(SharedPrefUtils.Key.IS_LANGUAGE_CHANGED, true)
            TrackingHelper.languageChangedConfirmButtonClicked()
            //fetch data from network.
            disposable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ result: BiModel.Result? -> saveResponse(GsonUtils.getInstance().serializeToJson(result)) })
                    { error: Throwable -> onFailure(error) }
            showNetworkDialog()
        }
    }

    private val observable: Observable<BiModel.Result>
        get() {
            return when (SharedPrefUtils.getInstance().getInt(SharedPrefUtils.Key.SELECTED_LANGUAGE)) {
                0 -> {
                    AppController.getInstance().subscribeToTopic("hindi")
                    AppController.getInstance().unSubscribeToTopic("english")
                    create().getDataHin()
                }
                1 -> create().getDataUr()
                else -> {
                    AppController.getInstance().subscribeToTopic("english")
                    AppController.getInstance().unSubscribeToTopic("hindi")
                    create().getDataEng()
                }
            }
        }

    private fun showNetworkDialog() {
        val fm = supportFragmentManager
        dialogFragment = NetworkDialog()
        dialogFragment!!.show(fm, "Network dialouge")
    }

    private fun dismissNetworkDialog() {
        AppController.dismissDialogFragment(this, dialogFragment)
        dialogFragment!!.dismiss()
    }

    fun onFailure(error: Throwable) {
        Log.d(TAG, "onFailure: " + error.message)
        dismissNetworkDialog()
        Toast.makeText(this, "Some thing went wrong. Please try again later.", Toast.LENGTH_SHORT).show()
    }

    private fun saveResponse(response: String) {
        val businessIdea = GsonUtils.getInstance().deserializeJSON<BusinessIdea>(response, BusinessIdea::class.java)
        if (businessIdea == null) {
            Toast.makeText(this, "Some thing went wrong. Please try again later.", Toast.LENGTH_SHORT).show()
            dismissNetworkDialog()
        } else {
            dismissNetworkDialog()
            openLandingActivity()
            SharedPrefUtils.getInstance().put(SharedPrefUtils.Key.LAST_LISTING_API_SUCCESS_TIMESTAMP, Date().time)
            Paper.book().write(AppConstants.IDEA_LISTING_DATA, response)
        }
        SharedPrefUtils.getInstance().put(SharedPrefUtils.Key.IS_LANGUAGE_CHANGED, false)
    }

    private fun openLandingActivity() {
        val i = Intent(this@SelectLanguageActivity, BusinessIdeaLandingActivity::class.java)
        startActivity(i)
        finish()
    }

    override fun onPause() {
        super.onPause()
        if (disposable != null) {
            disposable!!.dispose()
        }
    }

    companion object {
        private const val TAG = "SelectLanguageActivity"
    }
}