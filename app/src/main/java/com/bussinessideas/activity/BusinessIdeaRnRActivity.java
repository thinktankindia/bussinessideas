package com.bussinessideas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bussinessideas.R;
import com.bussinessideas.adapter.BusinessRnrListingAdapter;
import com.bussinessideas.ads.AdBuilder;
import com.bussinessideas.model.Chapter;
import com.bussinessideas.model.RnR;
import com.bussinessideas.util.AppConstants;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.BusinessIdeaRnrDetailActivity;
import com.bussinessideas.util.GsonUtils;
import com.google.android.gms.ads.AdView;

import java.util.List;

import static com.bussinessideas.util.AppConstants.DETAIL_TYPE;
import static com.bussinessideas.util.AppConstants.RNR;
import static com.bussinessideas.util.AppConstants.TIPS;

public class BusinessIdeaRnRActivity extends BaseActivity implements View.OnClickListener, AdBuilder.AdBuilderListener {

    private List<Chapter> chapters;
    private AdView mAdView;
    private AdBuilder adBuilder;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_idea_rn_r);
        type = getIntent().getIntExtra(DETAIL_TYPE, 0);
        initData();
        initViews();

        //Show ads.
        adBuilder = new AdBuilder(mAdView, this);
        adBuilder.showAds();
    }

    private void initViews() {

        mAdView = findViewById(R.id.adView_native_rnc);

        ImageView ivBack = findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);

        TextView tvListingHeaderText = findViewById(R.id.tv_rnc_header_text);
        tvListingHeaderText.setText(getResources().getString(R.string.business_idea));

        RecyclerView rvChapter = findViewById(R.id.rv_chapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        BusinessRnrListingAdapter adapter = new BusinessRnrListingAdapter(this, chapters);
        rvChapter.setLayoutManager(mLayoutManager);
        rvChapter.setItemAnimator(new DefaultItemAnimator());
        rvChapter.setAdapter(adapter);
    }

    private void initData() {
        String data = null;
        if (type == RNR) {
            data = AppController.getInstance().getRmRDataJSONFromAsset();
        } else if (type == TIPS) {
            data = AppController.getInstance().getTipsDataJSONFromAsset();
        }
        if (data != null) {
            RnR json = GsonUtils.getInstance().deserializeJSON(data, RnR.class);
            chapters = json.getChapter();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back) {
            onBackPressed();
        }
    }

    public void handleIdeaClicked(Object tag) {
        int position = (Integer) tag;
        Intent intent = new Intent(this, BusinessIdeaRnrDetailActivity.class);
        intent.putExtra(AppConstants.CHAPTER_DATA, chapters.get(position));
        startActivity(intent);
    }

    @Override
    public void onAdClosed() {

    }
}
