package com.bussinessideas.activity;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bussinessideas.R;
import com.bussinessideas.adapter.CustomListAdapter;
import com.bussinessideas.model.VideoDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChannelActivity extends AppCompatActivity {
    ListView lvVideo;
    List<VideoDetails> videoDetailsList;
    CustomListAdapter customListAdapter;
    private static final String TAG = "ChannelActivity";
    String CHANNEL_ID = "UCbi0zz_PZSdL-oZxob8n4oQ";
    String URL = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + CHANNEL_ID
            + "&maxResults=25&key=";
    private ImageView ivBack;
    private ProgressBar videoProgressBar;
    private View errorView;
    private Button btnError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);
        ivBack = findViewById(R.id.iv_back);
        videoProgressBar = findViewById(R.id.pb_video_progress);
        errorView = findViewById(R.id.error_view_channel);
        btnError = errorView.findViewById(R.id.btn_error_try_again);

        ivBack.setOnClickListener(v -> onBackPressed());
        btnError.setOnClickListener(v -> showVideo());

        URL = URL + getString(R.string.GOOGLE_API_KEY);
        lvVideo = findViewById(R.id.videoList);
        videoDetailsList = new ArrayList<>();
        customListAdapter = new CustomListAdapter(ChannelActivity.this, videoDetailsList);
        showVideo();
    }

    private void showVideo() {
        toggleVisibility(true);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, response -> {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    JSONObject jsonVideoId = jsonObject1.getJSONObject("id");
                    JSONObject jsonSnippet = jsonObject1.getJSONObject("snippet");
                    JSONObject jsonObjectDefault = jsonSnippet.getJSONObject("thumbnails").getJSONObject("medium");
                    VideoDetails videoDetails = new VideoDetails();

                    if (jsonVideoId.has("videoId")) {
                        String videoid = jsonVideoId.getString("videoId");
                        Log.d(TAG, " New Video Id" + videoid);
                        videoDetails.setVideoId(videoid);
                        videoDetails.setURL(jsonObjectDefault.getString("url"));
                        videoDetails.setVideoName(jsonSnippet.getString("title"));
                        videoDetails.setVideoDesc(jsonSnippet.getString("description"));
                        videoDetailsList.add(videoDetails);
                    }
                }

                lvVideo.setAdapter(customListAdapter);
                customListAdapter.notifyDataSetChanged();
                videoProgressBar.setVisibility(View.GONE);

            } catch (JSONException e) {
                Log.e(TAG, "showVideo: " + e.getMessage(), e);
                toggleVisibility(false);
            }

        }, e -> {
            Log.e(TAG, "showVideo: " + e.getMessage(), e);
            toggleVisibility(false);
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    private void toggleVisibility(boolean show) {
        if (show) {
            errorView.setVisibility(View.GONE);
            videoProgressBar.setVisibility(View.VISIBLE);
        } else {
            errorView.setVisibility(View.VISIBLE);
            videoProgressBar.setVisibility(View.GONE);
        }
    }
}
