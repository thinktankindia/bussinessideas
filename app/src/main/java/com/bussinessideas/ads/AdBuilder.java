package com.bussinessideas.ads;

import android.widget.Toast;

import com.bussinessideas.BuildConfig;
import com.bussinessideas.R;
import com.bussinessideas.util.AppController;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import static android.view.View.VISIBLE;

public class AdBuilder extends AdListener {
    private static AdView mAdView;
    private static final boolean DEBUG_ENABLED = BuildConfig.DEBUG;
    private InterstitialAd mInterstitialAd;
    private AdBuilderListener mListener;

    public AdBuilder(AdView adView, AdBuilderListener mListener) {
        mAdView = adView;
        this.mListener = mListener;
    }

    /**
     * Show Ad only to release builds.
     */
    public void showAds() {
        if (!DEBUG_ENABLED && mAdView != null) {
            AdRequest adRequest = getAdRequest();
            mAdView.loadAd(adRequest);
            mAdView.setAdListener(this);
        }
    }

    public static AdRequest getAdRequest(){
        return new AdRequest.Builder()
                .addTestDevice(AppController.getInstance().getContext().getResources().getString(R.string.TEST_DEVICE_ID_S4))
                .addTestDevice("1823ED76D66C2A4CC3FD45881F649217")
                .addTestDevice("96B193ABFAC7F105418B08771AB992D3")
                .build();
    }

    @Override
    public void onAdLoaded() {
        super.onAdLoaded();
        mAdView.setVisibility(VISIBLE);
    }

    public void pauseAd() {
        if (mAdView != null) {
            mAdView.pause();
        }
    }

    public void destroyAd() {
        if (mAdView != null) {
            mAdView.destroy();
        }
    }

    public void resumeAd() {
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    public void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and reload the ad.
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (DEBUG_ENABLED) {
            Toast.makeText(AppController.getInstance().getContext(), "Build Type: " + BuildConfig.BUILD_TYPE + ". Ad did not load", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadInterstitial(String adUnitId) {
        if (!DEBUG_ENABLED) {
            mInterstitialAd = newInterstitialAd(adUnitId);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("7C0AB91024D1CD4B9FBC12BE0A124CD9")
                    .addTestDevice("1823ED76D66C2A4CC3FD45881F649217")
                    .addTestDevice("96B193ABFAC7F105418B08771AB992D3")
                    .setRequestAgent("android_studio:ad_template").build();
            mInterstitialAd.loadAd(adRequest);
        }
    }


    public boolean isInterstitialAdLoaded() {
        return mInterstitialAd != null && mInterstitialAd.isLoaded();
    }

    private InterstitialAd newInterstitialAd(String adUnitId) {
        InterstitialAd interstitialAd = new InterstitialAd(AppController.getInstance().getContext());
        interstitialAd.setAdUnitId(adUnitId);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Implementation not required
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //implemenation not required.
            }

            @Override
            public void onAdClosed() {
                mListener.onAdClosed();
            }
        });
        return interstitialAd;
    }

    public interface AdBuilderListener {
        void onAdClosed();
    }
}