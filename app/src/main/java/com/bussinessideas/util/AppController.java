package com.bussinessideas.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.bussinessideas.BuildConfig;
import com.bussinessideas.network.NetworkUtil;
import com.bussinessideas.tracker.TrackingHelper;
import com.flurry.android.FlurryAgent;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static com.bussinessideas.util.AppConstants.IDEA_DATA_JSON;
import static com.bussinessideas.util.AppConstants.IDEA_LISTING_REQUEST;
import static com.bussinessideas.util.AppConstants.NETWORK_INFO_REQUEST;
import static com.bussinessideas.util.AppConstants.RNR_DATA_JSON;
import static com.bussinessideas.util.AppConstants.TIPS_DATA_JSON;
import static com.bussinessideas.util.DateUtil.getRoundedDifferenceInDays;
import static com.bussinessideas.util.SharedPrefUtils.Key.IS_LANGUAGE_CHANGED;
import static com.bussinessideas.util.SharedPrefUtils.Key.LAST_LISTING_API_SUCCESS_TIMESTAMP;
import static com.bussinessideas.util.SharedPrefUtils.Key.LAST_NETWORK_INFO_API_SUCCESS_TIMESTAMP;
import static com.bussinessideas.util.SharedPrefUtils.Key.SELECTED_LANGUAGE;


public class AppController extends Application {
    private static final String TAG = "AppController";
    private static final int MIN_DAYS_FOR_BOOK_REQUEST = 3;
    private static final String FLURRY_API_KEY = "T4SYZQHKR9K83JWDBMCB";
    private static final String UTF = "UTF-8";
    private static AppController sInstance;
    private static Application appInstance;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;

        //Init Flurry.
        new FlurryAgent.Builder()
                .withLogEnabled(false)
                .build(this, FLURRY_API_KEY);

        //you must anonymize the user data using a hashing function
        //such as MD5 or SHA256 prior to calling this method.
        FlurryAgent.setUserId(getDeviceId());

        //Init Network data if available.
        NetworkUtil.getInstance().populateNetworkInfoInLocalPaperDb();

        //Subscribe for test messaging service.
        subscribeToTopic();
    }

    public void subscribeToTopic(String topic) {
        if (StringUtil.isNotNullAndEmpty(topic)) {
            FirebaseMessaging.getInstance().subscribeToTopic(topic)
                    .addOnCompleteListener(task -> {
                        String msg = "Subscribed to topic " + topic;
                        if (!task.isSuccessful()) {
                            msg = "Subscription failed";
                        }
                        Log.d(TAG, msg);
                    });
        }
    }

    public void unSubscribeToTopic(String topic) {
        if (StringUtil.isNotNullAndEmpty(topic)) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
                    .addOnCompleteListener(task -> {
                        String msg = "UnSubscribed to topic " + topic;
                        if (!task.isSuccessful()) {
                            msg = "UnSubscription failed";
                        }
                        Log.d(TAG, msg);
                    });
        }
    }
    private void subscribeToTopic() {
        if (BuildConfig.DEBUG) {
            subscribeToTopic("test");
        }
    }


    public Context getContext() {
        return appInstance.getApplicationContext();
    }

    public static synchronized AppController getInstance() {
        if (sInstance == null) {
            sInstance = new AppController();
        }
        return sInstance;
    }

    @Nullable
    public String getIdeasDataJSONFromAsset() {
        String json;
        try {
            InputStream is = getContext().getAssets().open(IDEA_DATA_JSON);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, UTF);
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            return null;
        }
        return json;
    }

    @Nullable
    public String getRmRDataJSONFromAsset() {
        String json;
        try {
            InputStream is = getContext().getAssets().open(RNR_DATA_JSON);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, UTF);
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            return null;
        }
        return json;
    }

    @Nullable
    public String getTipsDataJSONFromAsset() {
        String json;
        try {
            InputStream is = getContext().getAssets().open(TIPS_DATA_JSON);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, UTF);
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            return null;
        }
        return json;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private long getLastSuccessApiResponseTime(int requestType) {
        if (requestType == IDEA_LISTING_REQUEST) {
            return SharedPrefUtils.getInstance().getLong(LAST_LISTING_API_SUCCESS_TIMESTAMP);
        } else if (requestType == NETWORK_INFO_REQUEST) {
            return SharedPrefUtils.getInstance().getLong(LAST_NETWORK_INFO_API_SUCCESS_TIMESTAMP);
        } else {
            return 0;
        }
    }

    public boolean isNetworkRequestRequiredForListing(int requestType) {
        boolean isRequired = false;
        //No calculation required if network is not available.
        if (!isNetworkAvailable()) {
            return isRequired;
        } else {
            long time_now = new Date().getTime();
            if (SharedPrefUtils.getInstance().getBoolean(IS_LANGUAGE_CHANGED) || getRoundedDifferenceInDays(time_now, getLastSuccessApiResponseTime(requestType)) > MIN_DAYS_FOR_BOOK_REQUEST) {
                isRequired = true;
            }
            return isRequired;
        }
    }

    public static Bitmap getCompressedBackground(int resId, int width, int height, Bitmap.Config config) {
        Bitmap bitmap = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inPreferredConfig = config;
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(AppController.getInstance().getContext().getResources(),
                resId, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image.
        int scaleFactor = (int) Math.max(1.0, Math.min((double) photoW / (double) width, (double) photoH / (double) height));
        scaleFactor = (int) Math.pow(2.0, Math.floor(Math.log((double) scaleFactor) / Math.log(2.0)));

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        do {
            try {
                scaleFactor *= 2;
                bitmap = BitmapFactory.decodeResource(AppController.getInstance().getContext().getResources(), resId, bmOptions);
            } catch (OutOfMemoryError e) {
                bmOptions.inSampleSize = scaleFactor;
                LogUtils.error(TAG, "OutOfMemoryError: trying to resize image " + scaleFactor, e);
            }
        }
        while (bitmap == null && scaleFactor <= 256);

        return bitmap;

    }

    public static void setLocale() {
        int langType = SharedPrefUtils.getInstance().getInt(SELECTED_LANGUAGE);
        switch (langType) {
            case 0:
                LocaleHelper.setLocale(AppController.getInstance().getContext(), "hi");
                break;
            case 1:
                LocaleHelper.setLocale(AppController.getInstance().getContext(), "ur");
                break;
            case 2:
                LocaleHelper.setLocale(AppController.getInstance().getContext(), "en");
                break;
            default:
                LocaleHelper.setLocale(AppController.getInstance().getContext(), "en");
                break;
        }
    }

    /**
     * This method checks if dialog fragment is active or not
     *
     * @param frag
     * @return
     */
    public static boolean isDialogFragmentUIActive(DialogFragment frag) {
        return (frag != null && frag.getActivity() != null && !frag.getActivity().isFinishing() && frag.getDialog() != null &&
                frag.getDialog().isShowing() && frag.isResumed());
    }

    /**
     * This method checks if dialog fragment is active or not
     *
     * @param frag
     * @return
     */
    public static boolean isFragmentUIActive(Fragment frag) {
        return (frag != null && frag.getActivity() != null && !frag.getActivity().isFinishing() &&
                frag.isResumed());
    }

    /**
     * This method dismiss a dialog fragment.
     *
     * @param activity
     * @param dialogFragment
     */
    public static void dismissDialogFragment(Activity activity, DialogFragment dialogFragment) {
        if (AppController.isDialogFragmentUIActive(dialogFragment)
                && null != activity && activity.isFinishing()) {
            dialogFragment.dismissAllowingStateLoss();
        }
    }

    public String getDeviceId() {
        return Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getAppLanguage() {
        int langType = SharedPrefUtils.getInstance().getInt(SELECTED_LANGUAGE);
        String language;
        switch (langType) {
            case 0:
                language = "Hindi";
                break;
            case 1:
                language = "Urdu";
                break;
            case 2:
                language = "English";
                break;
            case 3:
                language = "Chinese";
                break;
            default:
                language = "English";
                break;
        }
        return language;
    }

    public void shareApp(Activity context) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "101 Business Ideas");
            String sAux = "\nYou might want to know about businesses to get Rich. Let me recommend you this application\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.bussinessideas \n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, "101 Business Ideas"));
            TrackingHelper.shareIconClicked();
        } catch (Exception e) {
            LogUtils.error(TAG, e);
        }
    }

    public RequestQueue getRequestQueue() {
        if (this.mRequestQueue == null) {
            this.mRequestQueue = Volley.newRequestQueue(this.getContext());
        }
        return this.mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (this.mImageLoader == null) {
            this.mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        if (TextUtils.isEmpty(tag)) {
            tag = TAG;
        }
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (this.mRequestQueue != null) {
            this.mRequestQueue.cancelAll(tag);
        }
    }
}