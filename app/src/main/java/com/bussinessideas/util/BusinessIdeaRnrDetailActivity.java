package com.bussinessideas.util;

import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bussinessideas.activity.BaseActivity;
import com.bussinessideas.R;
import com.bussinessideas.ads.AdBuilder;
import com.bussinessideas.model.Chapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;

import java.util.Date;

import static com.bussinessideas.util.AppConstants.CHAPTER_DATA;
import static com.bussinessideas.util.AppConstants.MIN_TIME_TO_DISPLAY_INTERSTETIAL_AD;

public class BusinessIdeaRnrDetailActivity extends BaseActivity implements ViewTreeObserver.OnScrollChangedListener, AdBuilder.AdBuilderListener {

    private TextView tvChapterHeader;
    private TextView tvChapterContent;
    private AdBuilder adBuilder;
    private AdView mAdView;
    private long pageInitTime = 0;
    private FrameLayout flParallaxView;
    private FrameLayout svDetail;
    private boolean interstetialShowed = false;
    public NativeExpressAdView nativeExpressAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageInitTime = new Date().getTime();

        Chapter chapter = getIntent().getParcelableExtra(CHAPTER_DATA);
        setContentView(R.layout.activity_business_idea_rnr_detail);
        initViews();
        populateData(chapter);

        //Show ads.
        adBuilder = new AdBuilder(mAdView, this);
        adBuilder.showAds();

        //Load Interstetial Ad
        adBuilder.loadInterstitial(getString(R.string.interstitial_ad_unit_detail));

        //Show Native Ads.
        showNativeAd(nativeExpressAdView);
    }

    private void populateData(Chapter chapter) {
        tvChapterHeader.setText(chapter.getChapterName());
        tvChapterContent.setText(chapter.getChapterSummary());
    }


    private void initViews() {
        tvChapterHeader = (TextView) findViewById(R.id.tv_story_header);
        tvChapterContent = (TextView) findViewById(R.id.tv_chapter_content);
        mAdView = (AdView) findViewById(R.id.adView);
        flParallaxView = (FrameLayout) findViewById(R.id.fl_parallax_view);
        svDetail = (ScrollView) findViewById(R.id.sv_detail);
        nativeExpressAdView = (NativeExpressAdView) findViewById(R.id.adView_native_detail);

        //Attache scroll listener for parallaxed view
        attachScrollListener();
    }

    private void attachScrollListener() {
        svDetail.getViewTreeObserver().addOnScrollChangedListener(this);
    }

    @Override
    public void onBackPressed() {
        long clickTime = new Date().getTime();
        if (interstetialShowed || clickTime - pageInitTime < MIN_TIME_TO_DISPLAY_INTERSTETIAL_AD) {
            super.onBackPressed();
        } else {
            adBuilder.showInterstitial();
            interstetialShowed = true;
            finish();
        }
    }

    @Override
    public void onScrollChanged() {
        int scrollY = svDetail.getScrollY();
        parallaxEffectOnScroll(scrollY);
    }

    private void parallaxEffectOnScroll(int scrollY) {
        flParallaxView.setTranslationY(scrollY / 4.5f);
    }


    /**
     * This method updates the native ad view in the Adapter.
     *
     * @param nativeExpressAdView Native adview.
     */
    private void showNativeAd(final NativeExpressAdView nativeExpressAdView) {
        if (AppController.getInstance().isNetworkAvailable()) {
            nativeExpressAdView.loadAd(new AdRequest.Builder().build());
            nativeExpressAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    nativeExpressAdView.setVisibility(View.VISIBLE);
                }
            });
        } else {
            nativeExpressAdView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAdClosed() {
        //Implementation not required
    }
}
