package com.bussinessideas.util;

public class AppConstants {

    public final static String BI_LISTING_PAGE_LOADED = "listing_page_loaded";
    public final static String BI_LISTING_PAGE_ITEM_CLICKED = "listing_page_item_click";
    public final static String BI_DETAIL_PAGE_LOADED = "detail_page_shown";
    public final static String BI_DETAIL_PAGE_READ_MORE_CLICKED = "read_more_clicked_on_detail_page";

    public static final long MIN_TIME_TO_DISPLAY_INTERSTETIAL_AD = 30000;
    public static final java.lang.String IDEA_DATA_JSON = "business-ideas-data.json";
    public static final java.lang.String RNR_DATA_JSON = "rnr.json";
    public static final java.lang.String TIPS_DATA_JSON = "business_tips.json";

    public static final int IDEA_LISTING_REQUEST = 1;
    public static final int NETWORK_INFO_REQUEST = 2;

    public static final java.lang.String IDEA_LISTING_DATA = "idea-listing-data";
    public static final java.lang.String NETWORK_INFO_DATA = "network-info-data";

    public static final int NUM_ITEMS_DETAIL_PAGER = 3;
    public static final int SUMMARY_FRAGMENT = 0;
    public static final int DESCRIPTION_FRAGMENT = 1;
    public static final int EARNINGS_FRAGMENT = 2;
    public static final String CONTACT_US_FRAG = "contact_us_fragment";
    public static final String CHAPTER_DATA = "book_data";
    public static String TOP_IDEAS_IDS = "top_ideas";
    public static int RNR = 1;
    public static int TIPS = 2;
    public static String DETAIL_TYPE = "detail_type";
}