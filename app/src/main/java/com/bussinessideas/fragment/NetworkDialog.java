package com.bussinessideas.fragment;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.bussinessideas.R;
import com.bussinessideas.tracker.TrackingHelper;
import com.flurry.android.FlurryAgent;

import java.util.Map;

public class NetworkDialog extends DialogFragment {
    private static final String DIALOG_EVENT = "NETWORK_DIALOG_SHOWN";
    private Map<String, String> flurryParams = TrackingHelper.getDefaultParamsMap();

    static NetworkDialog newInstance(int num) {
        NetworkDialog networkDialog = new NetworkDialog();
        return networkDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlurryAgent.logEvent(DIALOG_EVENT, flurryParams, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.view_dialog_loading_from_network, container, false);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        FlurryAgent.endTimedEvent(DIALOG_EVENT);
    }
}
