package com.bussinessideas.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bussinessideas.R;
import com.bussinessideas.ads.AdBuilder;
import com.bussinessideas.customeviews.ExpandableTextViewFadedAtEnd;
import com.bussinessideas.model.Idea;
import com.bussinessideas.tracker.TrackingHelper;
import com.bussinessideas.util.StringUtil;
import com.google.android.gms.ads.AdView;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnDetailSummaryFragmentInteraction} interface
 * to handle interaction events.
 * Use the {@link DetailSummaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailSummaryFragment extends Fragment implements View.OnClickListener, AdBuilder.AdBuilderListener {


    private OnDetailSummaryFragmentInteraction mListener;
    private Idea idea;
    private ExpandableTextViewFadedAtEnd tvIdeaDescription;
    private TextView tvIdeaTitle;
    private TextView tvIdeaSummary;
    private LinearLayout llIdeaTags;
    private TextView tvReadFullDesc;
    private AdView adView;
    private AdBuilder adBuilder;

    public DetailSummaryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailSummaryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailSummaryFragment newInstance() {
        DetailSummaryFragment fragment = new DetailSummaryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        tvIdeaTitle = (TextView) v.findViewById(R.id.tv_idea_title);
        tvIdeaSummary = (TextView) v.findViewById(R.id.tv_idea_summary);
        tvIdeaDescription = (ExpandableTextViewFadedAtEnd) v.findViewById(R.id.tv_idea_description);
        llIdeaTags = (LinearLayout) v.findViewById(R.id.ll_idea_tags_on_detail);
        tvReadFullDesc = (TextView) v.findViewById(R.id.tv_read_full_desc);
        adView = (AdView) v.findViewById(R.id.adView);
        tvIdeaDescription.setOnClickListener(this);
        tvReadFullDesc.setOnClickListener(this);

        populateData();

        //Show ads.
        adBuilder = new AdBuilder(adView, this);
        adBuilder.showAds();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.detail_summary_fragment, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailSummaryFragmentInteraction) {
            mListener = (OnDetailSummaryFragmentInteraction) context;
            this.idea = mListener.getIdea();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDetailDescriptionFragmentInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAdClosed() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnDetailSummaryFragmentInteraction {
        Idea getIdea();
    }

    private void populateData() {
        if (idea != null
                && StringUtil.isNotNullAndEmpty(idea.getIdeaDesc())
                && StringUtil.isNotNullAndEmpty(idea.getTitleLong())
                && StringUtil.isNotNullAndEmpty(idea.getSummary())) {
            tvIdeaDescription.setText(idea.getIdeaDesc().trim());
            tvIdeaTitle.setText(idea.getTitleLong().trim());
            tvIdeaSummary.setText(idea.getSummary().trim());
            initTags(idea.getTags());
        }
    }

    private void initTags(List<String> tag) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (tag.size() > 0) {
            for (int i = 0; i < tag.size(); i++) {
                View view = inflater.inflate(R.layout.view_idea_tags, llIdeaTags, false);
                TextView tags = (TextView) view.findViewById(R.id.tv_idea_tag_detail);
                String tagLine = tag.get(i);
                if (tagLine != null && !tagLine.isEmpty()) {
                    tags.setText(tagLine);
                    llIdeaTags.addView(view);
                }
            }
            llIdeaTags.setVisibility(VISIBLE);
        }
    }


    private void expandIdeaDescription() {
        tvIdeaDescription.expand();
        tvReadFullDesc.setVisibility(GONE);
        //Tracking
        TrackingHelper.moreInfoClicked(idea);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_idea_description || v.getId() == R.id.tv_read_full_desc) {
            expandIdeaDescription();
        }
    }
}
