package com.bussinessideas.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bussinessideas.R;
import com.bussinessideas.model.Idea;
import com.bussinessideas.model.Investment;
import com.bussinessideas.tracker.TrackingHelper;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.LogUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnDetailDescriptionFragmentInteraction} interface
 * to handle interaction events.
 * Use the {@link DetailDescriptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailDescriptionFragment extends Fragment {
    private OnDetailDescriptionFragmentInteraction mListener;
    private Idea idea;
    private TextView tvInitialInvestment;
    private TextView tvSetupDuration;
    private TextView tvTotalManpower;
    private TextView tvWorkFromHome;
    private ImageView ivWfh;
    private AdView adView;
    private NativeExpressAdView adViewNativeDtail;


    public DetailDescriptionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailSummaryFragment.
     */
    public static DetailDescriptionFragment newInstance() {
        return new DetailDescriptionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        tvInitialInvestment = (TextView) v.findViewById(R.id.tv_initial_investment);
        tvSetupDuration = (TextView) v.findViewById(R.id.tv_setup_duration);
        tvTotalManpower = (TextView) v.findViewById(R.id.tv_total_manpower);
        tvWorkFromHome = (TextView) v.findViewById(R.id.tv_work_from_home);
        ivWfh = (ImageView) v.findViewById(R.id.iv_wfh);
        adView = (AdView) v.findViewById(R.id.adView);
        adViewNativeDtail = (NativeExpressAdView) v.findViewById(R.id.adView_native_detail);

        populateData();
    }

    private void populateData() {
        if (idea != null && idea.getInvestment() != null) {
            Investment investment = idea.getInvestment();
            //Initial investment required.
            tvInitialInvestment.setText(investment.getMoney());

            //Time Required.
            tvSetupDuration.setText(investment.getTime());

            //Man power Required.
            tvTotalManpower.setText(investment.getManpower());

            //work from home
            if (investment.getCanBeDoneFromHome()) {
                tvWorkFromHome.setVisibility(VISIBLE);
                ivWfh.setVisibility(VISIBLE);
            } else {
                tvWorkFromHome.setVisibility(GONE);
                ivWfh.setVisibility(GONE);
            }

            //Show Native Ads.
            showNativeAd(adViewNativeDtail);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_description_fragment, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailDescriptionFragmentInteraction) {
            mListener = (OnDetailDescriptionFragmentInteraction) context;
            this.idea = mListener.getIdea();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnDetailDescriptionFragmentInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnDetailDescriptionFragmentInteraction {
        Idea getIdea();
    }

    /**
     * This method updates the native ad view in the Adapter.
     *
     * @param nativeExpressAdView Native Express Ad view
     */
    private void showNativeAd(final NativeExpressAdView nativeExpressAdView) {
        if (AppController.getInstance().isNetworkAvailable()) {
            nativeExpressAdView.loadAd(new AdRequest.Builder().build());
            nativeExpressAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    nativeExpressAdView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    TrackingHelper.logNativeAdFailure("Detail Native Ad failed to load.");
                    LogUtils.error("native_ad_error", "Detail Native Ad failed to load.");
                }
            });
        } else {
            nativeExpressAdView.setVisibility(View.GONE);
        }
    }
}
