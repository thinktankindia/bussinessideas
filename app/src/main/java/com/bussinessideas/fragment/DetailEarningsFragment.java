package com.bussinessideas.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bussinessideas.R;
import com.bussinessideas.ads.AdBuilder;
import com.bussinessideas.model.ExpectedIncome;
import com.bussinessideas.model.Idea;
import com.bussinessideas.util.StringUtil;
import com.google.android.gms.ads.AdView;

import static android.view.View.GONE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnDetailEarningsFragmentInteraction} interface
 * to handle interaction events.
 * Use the {@link DetailEarningsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailEarningsFragment extends Fragment implements AdBuilder.AdBuilderListener {

    private OnDetailEarningsFragmentInteraction mListener;
    private Idea idea;
    private TextView tvEarnUpto;
    private ImageView ivEarn;
    private TextView tvTimeRequired;
    private TextView tvBestToStart;
    private LinearLayout llExtraInfo;
    private TextView tvGovtAidDetail;
    private TextView tvGovtAidContact;
    private RelativeLayout rlGovtAid;
    private AdBuilder adBuilder;
    private AdView adView;

    public DetailEarningsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailSummaryFragment.
     */
    public static DetailEarningsFragment newInstance() {
        return new DetailEarningsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_earnings_fragment, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        tvEarnUpto = (TextView) v.findViewById(R.id.tv_earn_upto);
        ivEarn = (ImageView) v.findViewById(R.id.iv_earn);
        tvTimeRequired = (TextView) v.findViewById(R.id.tv_time_required);
        tvBestToStart = (TextView) v.findViewById(R.id.tv_best_to_start);
        llExtraInfo = (LinearLayout) v.findViewById(R.id.ll_extra_info);
        tvGovtAidDetail = (TextView) v.findViewById(R.id.tv_govt_aid_detail);
        tvGovtAidContact = (TextView) v.findViewById(R.id.tv_govt_aid_contact);
        rlGovtAid = (RelativeLayout) v.findViewById(R.id.rl_govt_aid);
        adView = (AdView) v.findViewById(R.id.adView);

        initImpInfo();

        //Show ads.
        adBuilder = new AdBuilder(adView, this);
        adBuilder.showAds();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailEarningsFragmentInteraction) {
            mListener = (OnDetailEarningsFragmentInteraction) context;
            this.idea = mListener.getIdea();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnDetailEarningsFragmentInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAdClosed() {
        //Implementation not required.
    }

    public interface OnDetailEarningsFragmentInteraction {
        Idea getIdea();
    }

    private void initImpInfo() {
        //Expected income
        ExpectedIncome expectedIncome = idea.getExpectedIncome();

        if (expectedIncome != null) {
            if (StringUtil.isNotNullAndEmpty(expectedIncome.getExpectedMonthlyIncome())) {
                tvEarnUpto.setText(expectedIncome.getExpectedMonthlyIncome());
            } else {
                tvEarnUpto.setVisibility(GONE);
                ivEarn.setVisibility(GONE);
            }

            if (StringUtil.isNotNullAndEmpty(expectedIncome.getTimeRequiredToSetup())) {
                tvTimeRequired.setText(expectedIncome.getTimeRequiredToSetup());
            } else {
                tvTimeRequired.setVisibility(GONE);
            }
        } else {
            tvEarnUpto.setVisibility(GONE);
            tvTimeRequired.setVisibility(GONE);
        }


        //Best place to start business.
        tvBestToStart.setText(idea.getFavorableLocation());

        // Set extra Info if available.
        if (!idea.getExtras().isEmpty()) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for (int i = 0; i < idea.getExtras().size(); i++) {
                View view = inflater.inflate(R.layout.view_extra_info, llExtraInfo, false);
                TextView info = (TextView) view.findViewById(R.id.tv_extra_info);
                info.setText(idea.getExtras().get(i));
                llExtraInfo.addView(view);
            }
        }


        //Govt Aid
        if (idea.getGovernmentAid().isProvided()) {
            tvGovtAidDetail.setText(idea.getGovernmentAid().getDetail());
            tvGovtAidContact.setText(idea.getGovernmentAid().getContact());
        } else {
            rlGovtAid.setVisibility(GONE);
        }
    }

}