package com.bussinessideas.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.NetworkImageView
import com.bussinessideas.R
import com.bussinessideas.activity.VideoActivity
import com.bussinessideas.model.VideoDetails
import com.bussinessideas.util.AppController

class VideoAdapter(private var items: List<VideoDetails>, val context: VideoActivity) : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {
    private var imageLoader = AppController.getInstance().imageLoader

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.videolist, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.videoDesc.text = items[position].videoDesc
        holder.imgtitle.text = items[position].videoName
        holder.networkImageView.setImageUrl(items[position].url, imageLoader)

        holder.itemView.setOnClickListener {
            context.showVideo(items[position].videoId)
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var networkImageView: NetworkImageView = view.findViewById(R.id.video_image)
        var imgtitle: TextView = view.findViewById(R.id.video_title)
        var videoDesc: TextView = view.findViewById(R.id.video_descriptio)
    }

    fun updateList(items: List<VideoDetails>) {
        this.items = items
        notifyDataSetChanged()
    }
}