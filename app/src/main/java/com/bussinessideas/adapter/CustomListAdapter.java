package com.bussinessideas.adapter;


import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bussinessideas.R;
/*import com.bussinessideas.activity.VideoActivity;*/
import com.bussinessideas.model.VideoDetails;
import com.bussinessideas.util.AppController;

import java.util.ArrayList;
import java.util.List;

public class CustomListAdapter extends BaseAdapter {

    Activity activity;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private LayoutInflater inflater;
    List<VideoDetails> singletons;

    public CustomListAdapter(Activity activity, List<VideoDetails> singletons) {
        this.activity = activity;
        this.singletons = singletons;
    }

    public int getCount() {
        return this.singletons.size();
    }

    public Object getItem(int i) {
        return this.singletons.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (this.inflater == null) {
            this.inflater = this.activity.getLayoutInflater();
        }

        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.videolist, null);
        }

        if (this.imageLoader == null) {
            this.imageLoader = AppController.getInstance().getImageLoader();
        }

        NetworkImageView networkImageView = convertView.findViewById(R.id.video_image);

        final TextView imgtitle = convertView.findViewById(R.id.video_title);
        final TextView imgdesc = convertView.findViewById(R.id.video_descriptio);

        VideoDetails singleton = this.singletons.get(i);

      /*  convertView.findViewById(R.id.asser).setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), VideoActivity.class);
            intent.putExtra("videoId", singleton.getVideoId());
            intent.putParcelableArrayListExtra("video_list", (ArrayList<? extends Parcelable>) singletons);
            view.getContext().startActivity(intent);
        });*/

        networkImageView.setImageUrl(singleton.getURL(), this.imageLoader);
        imgtitle.setText(singleton.getVideoName());
        imgdesc.setText(singleton.getVideoDesc());
        return convertView;
    }
}