package com.bussinessideas.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bussinessideas.R;
import com.bussinessideas.fragment.DetailDescriptionFragment;
import com.bussinessideas.fragment.DetailEarningsFragment;
import com.bussinessideas.fragment.DetailSummaryFragment;
import com.bussinessideas.util.AppController;

import static com.bussinessideas.util.AppConstants.DESCRIPTION_FRAGMENT;
import static com.bussinessideas.util.AppConstants.EARNINGS_FRAGMENT;
import static com.bussinessideas.util.AppConstants.NUM_ITEMS_DETAIL_PAGER;
import static com.bussinessideas.util.AppConstants.SUMMARY_FRAGMENT;

/**
 * Created by Ashish on 23/12/16.
 */

public class DetailPagerAdapter extends FragmentPagerAdapter {

    public DetailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case SUMMARY_FRAGMENT:
                return DetailSummaryFragment.newInstance();
            case DESCRIPTION_FRAGMENT:
                return DetailDescriptionFragment.newInstance();
            case EARNINGS_FRAGMENT:
                return DetailEarningsFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS_DETAIL_PAGER;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return AppController.getInstance().getContext().getResources().getStringArray(R.array.detail_tabs)[position];
    }
}
