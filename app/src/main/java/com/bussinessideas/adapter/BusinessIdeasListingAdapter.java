package com.bussinessideas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bussinessideas.activity.BusinessIdeasListingActivity;
import com.bussinessideas.R;
import com.bussinessideas.model.Idea;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.StringUtil;
import com.google.android.gms.ads.AdLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.bussinessideas.activity.BusinessIdeasListingActivity.ITEMS_PER_AD;

public class BusinessIdeasListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private BusinessIdeasListingActivity mListener;
    private List<Idea> ideas;
    private static final String NATIVE_AD_UNIT_ID = "ca-app-pub-6114831955381407/1463344429";
    private AdLoader adLoader;
    private static final String TAG = "BusinessIdeasListingAda";

    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;

    // The Native Express ad view type.
    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;

    public BusinessIdeasListingAdapter(BusinessIdeasListingActivity businessIdeasLandingActivity, List<Idea> ideaData) {
        mListener = businessIdeasLandingActivity;
        this.ideas = ideaData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_single_idea, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);
        ViewHolder holder = (ViewHolder) viewHolder;
        Idea idea = ideas.get(position);

        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                showIdeaView(idea, holder, position);
                break;

            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                showIdeaView(idea, holder, position);
                showAdView(holder);
        }
    }

    /**
     * This updates the idea view of the adapter.
     *
     * @param idea
     * @param holder
     * @param position
     */
    private void showIdeaView(Idea idea, ViewHolder holder, int position) {
        String title = idea.getTitle();
        String tag = idea.getTags().get(0);
        String summary = idea.getSummary();
        String imageUrl = idea.getImgUrl();

        holder.title.setText(title);
        holder.tag.setText(tag);
        holder.summary.setText(summary);

        loadImage(imageUrl, holder.image);
        attachClickListener(holder, position);
    }


    /**
     * This method updates the native ad view in the Adapter.
     *
     * @param holder
     */
    private void showAdView(final ViewHolder holder) {
        if (AppController.getInstance().isNetworkAvailable()) {
            //adLoader.loadAd(new AdRequest.Builder().build());
        }
    }

    /**
     * Load image from networks and save it to cache.
     * From next time it will serve from cache.
     *
     * @param imageUrl Url of the Image
     * @param image    ImageView
     */
    private void loadImage(final String imageUrl, final ImageView image) {
        if (StringUtil.isNotNullAndEmpty(imageUrl)) {
            Picasso.get()
                    .load(imageUrl)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .error(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                    .placeholder(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Picasso.get()
                                    .load(imageUrl)
                                    .error(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                                    .placeholder(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                                    .into(image);
                        }
                    });
        }
    }

    private void attachClickListener(ViewHolder holder, int position) {
        holder.singleIdea.setTag(position);
        holder.singleIdea.setOnClickListener(this);
    }

    public void reloadList(List<Idea> idea) {
        this.ideas = idea;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return (position % ITEMS_PER_AD == 0 && AppController.getInstance().isNetworkAvailable()) ? NATIVE_EXPRESS_AD_VIEW_TYPE
                : MENU_ITEM_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return ideas.size();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_single_idea) {
            mListener.handleIdeaClicked(view.getTag());
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView summary;
        TextView tag;
        RelativeLayout singleIdea;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.iv_image);
            title = view.findViewById(R.id.tv_idea_header);
            summary = view.findViewById(R.id.tv_idea_summary);
            tag = view.findViewById(R.id.tv_idea_tag);
            singleIdea = view.findViewById(R.id.rl_single_idea);
        }
    }
}
