package com.bussinessideas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bussinessideas.activity.BusinessIdeaRnRActivity;
import com.bussinessideas.R;
import com.bussinessideas.model.Chapter;
import com.bussinessideas.tracker.TrackingHelper;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.LogUtils;
import com.bussinessideas.util.StringUtil;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.bussinessideas.activity.BusinessIdeasListingActivity.ITEMS_PER_AD;

public class BusinessRnrListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private BusinessIdeaRnRActivity mListener;
    private List<Chapter> chapters;

    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;

    // The Native Express ad view type.
    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;

    public BusinessRnrListingAdapter(BusinessIdeaRnRActivity rnRActivity, List<Chapter> chapters) {
        mListener = rnRActivity;
        this.chapters = chapters;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_rags_and_riches, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);
        ViewHolder holder = (ViewHolder) viewHolder;
        Chapter chapter = chapters.get(position);

        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                showIdeaView(chapter, holder, position);
                break;

            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                showIdeaView(chapter, holder, position);
                showAdView(holder);
        }
    }

    /**
     * This updates the Chapter view of the adapter.
     *
     * @param chapter
     * @param holder
     * @param position
     */
    private void showIdeaView(Chapter chapter, ViewHolder holder, int position) {
        String title = chapter.getChapterName();
        String summary = chapter.getChapterSummary();
        String imageUrl = chapter.getImageUrl();

        holder.title.setText(title);
        holder.summary.setText(summary);

        loadImage(imageUrl, holder.image);

        attachClickListener(holder, position);
        holder.adView.setVisibility(View.GONE);
    }


    /**
     * This method updates the native ad view in the Adapter.
     *
     * @param holder
     */
    private void showAdView(final ViewHolder holder) {
        if (AppController.getInstance().isNetworkAvailable()) {
            holder.adView.loadAd(new AdRequest.Builder().build());
            holder.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    holder.adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    TrackingHelper.logNativeAdFailure("Listing Native Ad failed to load.");
                    LogUtils.error("native_ad_error", "Listing Native Ad failed to load.");
                }
            });
        } else {
            holder.adView.setVisibility(View.GONE);
        }
    }

    /**
     * Load image from networks and save it to cache.
     * From next time it will serve from cache.
     *
     * @param imageUrl Url of the Image
     * @param image    ImageView
     */
    private void loadImage(final String imageUrl, final ImageView image) {
        if (StringUtil.isNotNullAndEmpty(imageUrl)) {
            Picasso.get()
                    .load(imageUrl)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .error(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                    .placeholder(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {
                            //Implementation not required.
                        }

                        @Override
                        public void onError(Exception e) {
                            Picasso.get()
                                    .load(imageUrl)
                                    .error(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                                    .placeholder(mListener.getApplicationContext().getResources().getDrawable(R.drawable.idea_single))
                                    .into(image);
                        }
                    });
        }
    }

    private void attachClickListener(ViewHolder holder, int position) {
        holder.singleIdea.setTag(position);
        holder.singleIdea.setOnClickListener(this);
    }

    public void reloadList(List<Chapter> chapters) {
        this.chapters = chapters;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return (position % ITEMS_PER_AD == 0 && AppController.getInstance().isNetworkAvailable()) ? NATIVE_EXPRESS_AD_VIEW_TYPE
                : MENU_ITEM_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        int listSize = chapters.size();
        return listSize;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_idea_view_rnc) {
            mListener.handleIdeaClicked(view.getTag());
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title;
        private TextView summary;
        private RelativeLayout singleIdea;
        private NativeExpressAdView adView;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.iv_image_rnc);
            title = view.findViewById(R.id.tv_idea_header_rnc);
            summary = view.findViewById(R.id.tv_idea_summary_rnc);
            singleIdea = view.findViewById(R.id.rl_idea_view_rnc);
            adView = view.findViewById(R.id.adView_native_rnc);
        }
    }
}
