package com.bussinessideas.network

import com.bussinessideas.model.BiModel
import dagger.Provides
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

interface BiService {
    @GET("/business-ideas-data_hi.json")
    fun getDataHin(): Observable<BiModel.Result>

    @GET("/business-ideas-data_eng.json")
    fun getDataEng(): Observable<BiModel.Result>

    @GET("/business-ideas-data_eng.json")
    fun getDataUr(): Observable<BiModel.Result>

    companion object {
        fun create(): BiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                   // .client(getUnsafeOkHttpClient())
                    .baseUrl("https://admob-app-id-9589461173.firebaseapp.com/")
                    .build()
            return retrofit.create(BiService::class.java)
        }

        @Provides
        @Singleton
        fun getUnsafeOkHttpClient(): OkHttpClient {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val builder = OkHttpClient.Builder()
            return builder.addInterceptor(interceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .followRedirects(true)
                    .followSslRedirects(true)
                    .build()
        }
    }
}