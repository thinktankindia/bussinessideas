package com.bussinessideas.network;


import com.bussinessideas.util.SharedPrefUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Request;

import static com.bussinessideas.util.AppConstants.IDEA_LISTING_REQUEST;
import static com.bussinessideas.util.AppConstants.NETWORK_INFO_REQUEST;
import static com.bussinessideas.util.SharedPrefUtils.Key.SELECTED_LANGUAGE;

public class RequestGenerator {
    //private final static String DOMAIN_CONNECT_URL_WITH_PROTOCOL = "http://thinktankindia.site88.net";
    private final static String DOMAIN_CONNECT_URL_WITH_PROTOCOL = "https://admob-app-id-9589461173.firebaseapp.com";
    private final static String IDEA_LISTING_URL_HI = DOMAIN_CONNECT_URL_WITH_PROTOCOL + "/business-ideas-data_hi.json";
    private final static String IDEA_LISTING_URL_ENG = DOMAIN_CONNECT_URL_WITH_PROTOCOL + "/business-ideas-data_eng.json";
    private final static String IDEA_LISTING_URL_UR = DOMAIN_CONNECT_URL_WITH_PROTOCOL + "/business-ideas-data_ur.json";
    private final static String IDEA_LISTING_URL_CH = DOMAIN_CONNECT_URL_WITH_PROTOCOL + "/business-ideas-data_ch.json";
    private final static String NETWORK_INFO_URL = "http://www.ip-api.com/json";
    private static final int MAX_CACHE_DAYS = 1;
    private Request.Builder mRequestBuilder;

    /**
     * This method build HTTP request.
     * Also set URL based on the request type.
     *
     * @param requestType type of the request. Defined in App constant.
     * @param isCached    if http caching required.
     * @return build a http Request.
     */
    public Request buildRequest(int requestType, boolean isCached) {
        Request request = null;
        if (requestType == IDEA_LISTING_REQUEST) {
            int langType = SharedPrefUtils.getInstance().getInt(SELECTED_LANGUAGE);
            switch (langType) {
                case 0:
                    request = getRequestBuilder(isCached).tag(requestType).url(IDEA_LISTING_URL_HI).build();
                    break;
                case 1:
                    request = getRequestBuilder(isCached).tag(requestType).url(IDEA_LISTING_URL_UR).build();
                    break;
                case 2:
                    request = getRequestBuilder(isCached).tag(requestType).url(IDEA_LISTING_URL_ENG).build();
                    break;
                case 3:
                    request = getRequestBuilder(isCached).tag(requestType).url(IDEA_LISTING_URL_CH).build();
                    break;
                default:
                    request = getRequestBuilder(isCached).tag(requestType).url(IDEA_LISTING_URL_ENG).build();
                    break;
            }
        } else if (requestType == NETWORK_INFO_REQUEST) {
            request = getRequestBuilder(isCached).tag(requestType).url(NETWORK_INFO_URL).build();
        }
        return request;
    }

    private Request.Builder getRequestBuilder(boolean isCached) {
        //Check if request builder is null.
        if (mRequestBuilder == null) {
            mRequestBuilder = new Request.Builder();
        }

        //Set cache type and duration in request builder.
        if (isCached) {
            mRequestBuilder.cacheControl(new CacheControl.Builder().onlyIfCached().maxStale(MAX_CACHE_DAYS, TimeUnit.DAYS).build());
        } else {
            mRequestBuilder.cacheControl(new CacheControl.Builder().noCache().maxStale(MAX_CACHE_DAYS, TimeUnit.DAYS).build());
        }
        return mRequestBuilder;
    }
}