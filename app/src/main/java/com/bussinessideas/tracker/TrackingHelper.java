package com.bussinessideas.tracker;


import com.bussinessideas.BuildConfig;
import com.bussinessideas.model.Idea;
import com.bussinessideas.model.NetworkDTO;
import com.bussinessideas.network.NetworkUtil;
import com.bussinessideas.util.AppController;
import com.bussinessideas.util.DateUtil;
import com.bussinessideas.util.LogUtils;
import com.flurry.android.FlurryAgent;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TrackingHelper {

    private static final String LOG_TAG = "TrackingHelper";
    public static final boolean TRACKING_DISABLED = BuildConfig.DEBUG;
    private static final String TAG = TrackingHelper.class.getSimpleName();

    private static void logEventToFlurry(String eventName, Map<String, String> params) {
        if (!TRACKING_DISABLED) {
            FlurryAgent.logEvent(eventName, params);
        } else {
            LogUtils.info(LOG_TAG, "Tracking disabled for build type: " + BuildConfig.BUILD_TYPE);
        }
    }

    public static void listingPageLoaded() {
        Map<String, String> params = getDefaultParamsMap();
        logEventToFlurry("LISTING_PAGE_LOADED", params);
    }

    public static void onListingPageItemClicked(String title, int ideaPosition) {
        Map<String, String> params = getDefaultParamsMap();
        params.put("Idea_title", title);
        params.put("Idea_position", String.valueOf(ideaPosition));
        logEventToFlurry("LISTING_PAGE_ITEM_CLICKED", params);
    }

    public static void changeLanguageIconClicked() {
        Map<String, String> params = getDefaultParamsMap();
        logEventToFlurry("CHANGE_LANGUAGE_ICON_CLICKED", params);
    }


    public static void languageChangedConfirmButtonClicked() {
        Map<String, String> params = getDefaultParamsMap();
        logEventToFlurry("LANGUAGE_CONFIRM_BUTTON_CLICKED", params);
    }

    public static void shareIconClicked() {
        Map<String, String> params = getDefaultParamsMap();
        params.put("App share icon click", AppController.getInstance().getDeviceId());
        logEventToFlurry("SHARE_ICON_CLICKED", params);
    }

    public static void moreInfoClicked(Idea idea) {
        try {
            Map<String, String> params = getDefaultParamsMap();
            params.put("Idea_title", idea.getTitle());
            logEventToFlurry("MORE_INFO_DETAIL_CLICKED", params);
        } catch (Exception e) {
            LogUtils.error(TAG, e);
        }

    }

    public static void logNativeAdFailure(String reason) {
        Map<String, String> params = getDefaultParamsMap();
        params.put("error", reason);
        logEventToFlurry("NATIVE_AD_ERROR", params);
    }
    /**
     * Log network data to flurry.
     *
     * @param networkDTO
     */
    public static void logNetworkData(NetworkDTO networkDTO) {
        try {
            // Capture author info & user status
            Map<String, String> networkParams = new HashMap<>();

            //param keys and values have to be of String type
            networkParams.put("Date", DateUtil.getCurrentDate());
            networkParams.put("City", networkDTO.getCity());
            networkParams.put("Amazon_Country", networkDTO.getCountry());
            networkParams.put("ISP", networkDTO.getIsp());
            networkParams.put("Zip", networkDTO.getZip());
            networkParams.put("Organisation", networkDTO.getOrg());
            networkParams.put("Region", networkDTO.getRegion() + networkDTO.getRegionName());

            //Flurry uses cached value (to avoid excessive battery usage) when location reporting is enabled.
            FlurryAgent.setLocation(doubleToFloat(networkDTO.getLat()), doubleToFloat(networkDTO.getLon()));

            //up to 10 params can be logged with each event
            logNetworkEventToFlurry("NETWORK_INFO", networkParams);

        } catch (Exception e) {
            FlurryAgent.onError("ERROR", "log_network_data", e);
        }
    }

    private static void logNetworkEventToFlurry(String eventName, Map<String, String> params) {
        if (!TRACKING_DISABLED) {
            //up to 10 params can be logged with each event
            FlurryAgent.logEvent(eventName, params);
        } else {
            LogUtils.info(LOG_TAG, "Tracking disabled for build type: " + BuildConfig.BUILD_TYPE);
        }
    }

    public static Map<String, String> getDefaultParamsMap() {
        Map<String, String> params = new HashMap<>();
        //up to 10 params can be logged with each event
        try {
            params.put("Country", NetworkUtil.getInstance().getCountry());
            params.put("Network_state", NetworkUtil.getInstance().getNetworkState());
            params.put("Device_id", AppController.getInstance().getDeviceId());
            params.put("App_language", AppController.getInstance().getAppLanguage());
        } catch (Exception e) {
            FlurryAgent.onError("ERROR", "default_param", e);
        }
        return params;
    }


    /**
     * Converts Double to float
     *
     * @param val double value
     * @return float value
     */
    private static float doubleToFloat(Double val) {
        return new BigDecimal(val).floatValue();
    }

}