package com.bussinessideas.customeviews;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;

/*
 * Created by Ankit on 6/21/2015.

    Expandable text view with alpha blended text at the end in the collapsed state

    For expanding , call expand function

    Needs:
    1) SetText should be called from ViewTreeObserver.OnGlobalLayoutListener()
    2) Max lines need to be set
 */

public class ExpandableTextViewFadedAtEnd extends TextView {

    private CharSequence originalText;
    private CharSequence trimmedText;
    private BufferType bufferType;
    private boolean trim = true;
    private boolean isExpandable = false;

    public ExpandableTextViewFadedAtEnd(Context context) {
        this(context, null);
    }

    public ExpandableTextViewFadedAtEnd(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void setText() {
        super.setText(getDisplayableText(), bufferType);
    }

    private CharSequence getDisplayableText() {
        return trim ? trimmedText : originalText;
    }

    public void expand() {
        ObjectAnimator animation = ObjectAnimator.ofInt(this, "maxLines", 500);
        animation.setDuration(3000).start();
        trim = !trim;
        setText();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        trimmedText = getTrimmedText(text);
        bufferType = type;
        setText();
    }

    /*BackBone of this class. Gets the trimmed char sequence*/
    private CharSequence getTrimmedText(CharSequence text) {
        if (originalText != null && !originalText.equals("") && getMeasuredWidth() != 0) {

            int charsInOneLine = Math.abs(this.getPaint().breakText(text, 0, text.length(), true, getMeasuredWidth(), null));

            //As not all can fit in a line due to text wrapping. Check how many chars can fit in getMaxLines()
            int start = 0;

            for (int i = 0; i < getMaxLines(); i++) {
                if (start < text.length()) {
                    String str = "";

                    if ((charsInOneLine + start) < text.length()) {
                        str = text.subSequence(start, charsInOneLine + start).toString();
                    } else {
                        str = text.subSequence(start, text.length()).toString();
                    }

                    if (!str.contains("\n")) {
                        int lastIndexOfSpace = str.lastIndexOf(" ");
                        //If space at end or in the midddle

                        if ((charsInOneLine + start) < text.length()) {
                            if (' ' != text.charAt(charsInOneLine + start) && lastIndexOfSpace != -1) {
                                start += lastIndexOfSpace + 1;
                            } else {
                                start += charsInOneLine + 1;
                            }
                        } else {
                            start = text.length();
                        }
                    } else {
                        //if it has enter
                        int enterIndex = str.indexOf('\n');
                        start += enterIndex + 1;
                    }
                }
            }

            int totalCharsToShow = start;

            //Check whether needs to expand
            if (totalCharsToShow != 0 && totalCharsToShow < text.length()) {
                isExpandable = true;
            } else {
                isExpandable = false;
            }

            SpannableStringBuilder builder = new SpannableStringBuilder(text, 0, totalCharsToShow);
            Spannable wordToSpan = new SpannableString(builder);

            int length = wordToSpan.length();

            if (isExpandable && length > 8) {
                //Modify the alpha of last 8 chars
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.9f)), length - 8, length - 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.8f)), length - 7, length - 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.7f)), length - 6, length - 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.6f)), length - 5, length - 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.5f)), length - 4, length - 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.3f)), length - 3, length - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.2f)), length - 2, length - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(adjustAlpha(getCurrentTextColor(), 0.1f)), length - 1, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            return wordToSpan;

        } else {
            return originalText;
        }
    }

    /*Returns the alpha modified color based on the factor supplied*/
    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public boolean isExpandable() {
        return isExpandable;
    }
}